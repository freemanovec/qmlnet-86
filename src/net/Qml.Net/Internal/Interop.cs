using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using NetNativeLibLoader.Loader;
using NetNativeLibLoader.PathResolver;
using Qml.Net.Internal.Qml;
using Qml.Net.Internal.Types;

namespace Qml.Net.Internal
{
    internal static class Interop
    {
        static readonly CallbacksImpl DefaultCallbacks = new CallbacksImpl(new DefaultCallbacks());

        static Interop()
        {
            Console.WriteLine($"Interop constructing, iter. confrontation unlikely separation");

            IPathResolver pathResolver = null;
            IPlatformLoader loader = null;

            if (Host.GetExportedSymbol != null)
            {
                // We are loading exported functions from the currently running executable.
                Console.WriteLine($"Host exported symbol isn't null, using host's loader");
                pathResolver = new Host.Loader();
                loader = new Host.Loader();
            }
            else
            {
                Console.WriteLine($"Creating dynamic library path resolver");
                pathResolver = new DynamicLinkLibraryPathResolver();
                loader = PlatformLoaderBase.SelectPlatformLoader();
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    // This custom path resolver attempts to do a DllImport to get the path that .NET decides.
                    // It may load a special dll from a NuGet package.
                    Console.WriteLine($"Specifying to Windows Path Resolver");
                    pathResolver = new WindowsDllImportLibraryPathResolver(pathResolver);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Console.WriteLine($"Specifying to OSX Path Resolver");
                    pathResolver = new MacDllImportLibraryPathResolver(pathResolver);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Console.WriteLine($"Specifying to Linux Path Resolver");
                    pathResolver = new LinuxDllImportLibraryPathResolver(pathResolver);
                }
            }

            var result = pathResolver.Resolve("QmlNet");

            Console.WriteLine($"Path resolver result: {result.Path}");

            if (!result.IsSuccess)
            {
                throw new Exception("Unable to find the native Qml.Net library." +
                                    " Try calling \"RuntimeManager.DiscoverOrDownloadSuitableQtRuntime();\" in Program.Main()");
            }

            Console.WriteLine($"Loading library");
            var library = loader.LoadLibrary(result.Path);
            Console.WriteLine($"Loading interop Callbacks");
            Callbacks = LoadInteropType<CallbacksInterop>(library, loader);
            Console.WriteLine($"Loading interop NetTypeInfo");
            NetTypeInfo = LoadInteropType<NetTypeInfoInterop>(library, loader);
            Console.WriteLine($"Loading interop NetJsValue");
            NetJsValue = LoadInteropType<NetJsValueInterop>(library, loader);
            Console.WriteLine($"Loading interop NetMethodInfo");
            NetMethodInfo = LoadInteropType<NetMethodInfoInterop>(library, loader);
            Console.WriteLine($"Loading interop NetPropertyInfo");
            NetPropertyInfo = LoadInteropType<NetPropertyInfoInterop>(library, loader);
            Console.WriteLine($"Loading interop NetTypeManager");
            NetTypeManager = LoadInteropType<NetTypeManagerInterop>(library, loader);
            Console.WriteLine($"Loading interop QCoreApplication");
            QCoreApplication = LoadInteropType<QCoreApplicationInterop>(library, loader);
            Console.WriteLine($"Loading interop QQmlApplicationEngine");
            QQmlApplicationEngine = LoadInteropType<QQmlApplicationEngineInterop>(library, loader);
            Console.WriteLine($"Loading interop NetVariant");
            NetVariant = LoadInteropType<NetVariantInterop>(library, loader);
            Console.WriteLine($"Loading interop NetReference");
            NetReference = LoadInteropType<NetReferenceInterop>(library, loader);
            Console.WriteLine($"Loading interop NetVariantList");
            NetVariantList = LoadInteropType<NetVariantListInterop>(library, loader);
            Console.WriteLine($"Loading interop NetTestHelper");
            NetTestHelper = LoadInteropType<NetTestHelperInterop>(library, loader);
            Console.WriteLine($"Loading interop NetSignalInfo");
            NetSignalInfo = LoadInteropType<NetSignalInfoInterop>(library, loader);
            Console.WriteLine($"Loading interop QResource");
            QResource = LoadInteropType<QResourceInterop>(library, loader);
            Console.WriteLine($"Loading interop NetDelegate");
            NetDelegate = LoadInteropType<NetDelegateInterop>(library, loader);
            Console.WriteLine($"Loading interop QQuickStyle");
            QQuickStyle = LoadInteropType<QQuickStyleInterop>(library, loader);
            Console.WriteLine($"Loading interop QtInterop");
            QtInterop = LoadInteropType<QtInterop>(library, loader);
            Console.WriteLine($"Loading interop Utilities");
            Utilities = LoadInteropType<UtilitiesInterop>(library, loader);
            Console.WriteLine($"Loading interop QtWebEngine");
            QtWebEngine = LoadInteropType<QtWebEngineInterop>(library, loader);
            Console.WriteLine($"Loading interop QTest");
            QTest = LoadInteropType<QTestInterop>(library, loader);
            Console.WriteLine($"Loading interop NetQObject");
            NetQObject = LoadInteropType<NetQObjectInterop>(library, loader);
            Console.WriteLine($"Loading interop NetQObjectSignalConnection");
            NetQObjectSignalConnection = LoadInteropType<NetQObjectSignalConnectionInterop>(library, loader);
            Console.WriteLine("Interop type loading done");

            // RuntimeManager.ConfigureRuntimeDirectory may set these environment variables.
            // However, they only really work when called with Qt.PutEnv.
            Qt.PutEnv("QT_PLUGIN_PATH", Environment.GetEnvironmentVariable("QT_PLUGIN_PATH"));
            Qt.PutEnv("QML2_IMPORT_PATH", Environment.GetEnvironmentVariable("QML2_IMPORT_PATH"));

            Console.WriteLine($"Registering default callbacks");
            var cb = DefaultCallbacks.Callbacks();
            Callbacks.RegisterCallbacks(ref cb);
        }

        public static CallbacksInterop Callbacks { get; }

        public static NetTypeInfoInterop NetTypeInfo { get; }

        public static NetMethodInfoInterop NetMethodInfo { get; }

        public static NetPropertyInfoInterop NetPropertyInfo { get; }

        public static NetTypeManagerInterop NetTypeManager { get; }

        public static QCoreApplicationInterop QCoreApplication { get; }

        public static QQmlApplicationEngineInterop QQmlApplicationEngine { get; }

        public static NetVariantInterop NetVariant { get; }

        public static NetReferenceInterop NetReference { get; }

        public static NetVariantListInterop NetVariantList { get; }

        public static NetTestHelperInterop NetTestHelper { get; }

        public static NetSignalInfoInterop NetSignalInfo { get; }

        public static QResourceInterop QResource { get; }

        public static NetDelegateInterop NetDelegate { get; }

        public static NetJsValueInterop NetJsValue { get; }

        public static QQuickStyleInterop QQuickStyle { get; }

        public static QtInterop QtInterop { get; }

        public static UtilitiesInterop Utilities { get; }

        public static QtWebEngineInterop QtWebEngine { get; }

        public static QTestInterop QTest { get; }
        
        public static NetQObjectInterop NetQObject { get; }
        
        public static NetQObjectSignalConnectionInterop NetQObjectSignalConnection { get; set; }

        private static T LoadInteropType<T>(IntPtr library, IPlatformLoader loader)
            where T : new()
        {
            Console.WriteLine($"LoadInteropType called with type argument of {typeof(T).FullName}");
            var result = new T();
            LoadDelegates(result, library, loader);
            return result;
        }

        private static void LoadDelegates(object o, IntPtr library, NetNativeLibLoader.Loader.IPlatformLoader loader)
        {
            foreach (var property in o.GetType().GetProperties())
            {
                var entryName = property.GetCustomAttributes().OfType<NativeSymbolAttribute>().First().Entrypoint;
                Console.WriteLine($"Detected attribute on property {property.Name} with entrypoint set to {entryName}");
                var symbol = loader.LoadSymbol(library, entryName);
                if(symbol == IntPtr.Zero)
                    Console.WriteLine("  - FAIL");
                else
                    Console.WriteLine("  - ok");
                property.SetValue(o, Marshal.GetDelegateForFunctionPointer(symbol, property.PropertyType));
            }
        }
    }
}