﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using NetNativeLibLoader.PathResolver;

namespace Qml.Net.Internal
{
    internal class WindowsDllImportLibraryPathResolver : IPathResolver
    {
        IPathResolver _original;

        public WindowsDllImportLibraryPathResolver(IPathResolver original)
        {
            Console.WriteLine("Constructed a windows PathResolver");
            _original = original;
        }

        public ResolvePathResult Resolve(string library)
        {
            Console.WriteLine($"Resolving: {library}");
            if (_original is DynamicLinkLibraryPathResolver _originalResolver)
            {
                Console.WriteLine($"Original is a DynamicLinkLibraryPathResolver");
                MethodInfo _getCandidatesMethod = _originalResolver.GetType().GetMethod("GenerateLibraryCandidates", BindingFlags.NonPublic | BindingFlags.Static);
                if (_getCandidatesMethod != null)
                {
                    Console.WriteLine($"Successfuly retrieved the GetLibraryCandidates reference");
                    Console.WriteLine($"Retrieving candidates for {library}");
                    object _invocationResult = _getCandidatesMethod.Invoke(_originalResolver, new object[] { "QmlNet" });
                    if (_invocationResult is IEnumerable<string> _invocationResultEnumerable)
                    {
                        Console.WriteLine($"Invocation result is of desired enumerable type, enumerating now");
                        foreach (string _s in _invocationResultEnumerable)
                            Console.WriteLine($" - {_s}");
                    }
                }
                if (AppContext.GetData("NATIVE_DLL_SEARCH_DIRECTORIES") is string directories)
                {
                    Console.WriteLine($"Native search directories are set: {directories}");
                }
            }
            var result = _original.Resolve(library);
            Console.WriteLine($"Originally resolved as: {result.Path}");

            if (!result.IsSuccess && library == "QmlNet")
            {
                Console.WriteLine("Resolve failed and we're trying to resolve QmlNet");
                // Try to let .NET load the library.
                try
                {
                    qml_net_getVersion();
                    Console.WriteLine($"Successfuly called qml_net_getVersion()");

                    // The method invoked correctly, so .NET loaded it.
                    // Let's return the path to it.
                    var loaded = GetModuleHandle("QmlNet");

                    var bytes = Marshal.AllocHGlobal(2000);
                    var r = GetModuleFileName(loaded, bytes, 2000);
                    var path = Marshal.PtrToStringAnsi(bytes);
                    Marshal.FreeHGlobal(bytes);

                    if (File.Exists(path))
                    {
                        Console.WriteLine($"Success, retrieved path: {path}");
                        return ResolvePathResult.FromSuccess(path);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception during call to qml_net_getVersion(), details follow");
                    Console.WriteLine(e);
                }
            }

            return result;
        }

        [DllImport("QmlNet")]
        internal static extern long qml_net_getVersion();

        [DllImport("kernel32.dll")]
        public static extern uint GetModuleFileName([In]IntPtr hModule, [Out]IntPtr lpFilename, [In, MarshalAs(UnmanagedType.U4)]int nSize);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);
    }
}
